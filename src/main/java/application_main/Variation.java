package application_main;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component
public class Variation {
     @Autowired
	public List <String>variationList;
	
	public String toString () {
		StringBuilder sb=new StringBuilder();
		variationList.forEach(str -> sb.append(str+""));
		return sb.toString();
		
	}
	public void add(String VariationName) {
		variationList.add(VariationName);
	}
}